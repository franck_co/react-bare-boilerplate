import { history } from 'app/history'
import { Dot } from 'core/com'
import { Circle } from './com/circle'
import './page2.module.css'


function handleClick(){
    history.push('./page1')
}

export function Page2() {
    
    return (
        <div>
            <div>niveau 2</div>
            <Dot color="green"/>
            <button onClick={() => handleClick()}>go to page1</button>
            <Circle/>
        </div>
    )
}