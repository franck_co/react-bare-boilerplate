import { history } from 'app/history'
import { Dot } from 'core/com'
import { Square } from './com/square'
import './page1.module.css'


function handleClick(){
    history.push('./page2')
}

export function Page1() {
    
    return (
        <div>
            <div>niveau 1</div>
            <Dot color="green"/>
            <button onClick={() => handleClick()}>go to page2</button>
            <Square/>
        </div>
    )
}