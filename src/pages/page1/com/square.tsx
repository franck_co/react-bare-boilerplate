import { CSSProperties, useState } from 'react'


interface state  {
    color: string
    position:[x:number,y:number]
    score:number
}

export function Square() {

    const [state,setState] = useState<state>({color:'black', position:[50,50],score:0})

    const [x,y] = state.position
    const style:CSSProperties = {
        height:"100px",
        width:"100px",
        backgroundColor:state.color,
        position:"absolute",
        left:`${x}%`,
        top:`${y}%`,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        fontSize:'xxx-large',
        fontFamily:'cursive'
    }

    function  handleClick(){
        const r = Math.round(Math.random()*255)
        const v = Math.round(Math.random()*255)
        const b = Math.round(Math.random()*255)
  

        const x = Math.round(Math.random()*95)
        const y = Math.round(Math.random()*95)
        
        setState({...state,color:`rgb(${r},${v},${b})`,position:[x,y], score:state.score + 1})
    }


    return (
        <div style = {style} onClick={handleClick}>{state.score}</div>
    
    )
}