type FilterFlagsAdd<Base, Condition> = {
    [Key in keyof Base]:
    Base[Key] extends Condition ? Key : never
};



type AllowedNamesAdd<Base, Condition> =
    FilterFlagsAdd<Base, Condition>[keyof Base];

export type SubTypeSelect<Base, Condition> =
    Pick<Base, AllowedNamesAdd<Base, Condition>>;


type FilterFlagsRemove<Base, Condition> = {
    [Key in keyof Base]:
    Base[Key] extends Condition ? never : Key
};

type AllowedNamesRemove<Base, Condition> =
    FilterFlagsRemove<Base, Condition>[keyof Base];

export type SubTypeRemove<Base, Condition> =
    Pick<Base, AllowedNamesRemove<Base, Condition>>;