export const ConditionalWrapper = ({ condition, wrapper, children }) => 
  condition ? wrapper(children) : children;


export function jsxJoin (array, str) {
  return array.length > 0
    ? array.reduce((acc, item) => <>{acc}{str}{item}</>)
    : null;
}
