type Nullable<T> = T | null;
type FreeObj<T = void> = T  extends void ? Record<string | symbol,any> : Record<string | symbol,any> & T

type FilterFlagsAdd<Base, Condition> = {
    [Key in keyof Base]:
    Base[Key] extends Condition ? Key : never
};
